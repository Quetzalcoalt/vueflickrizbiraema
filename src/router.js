import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'
import User from './views/User.vue'
import Image from './views/ImageDetails.vue'

Vue.use(Router)

function loadView(view) {
    return () =>
        import(/* webpackChunkName: "view-[request]" */ `./views/${view}.vue`)
}

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: () =>
                import(/* webpackChunkName: "group-foo" */ './views/Home.vue')
        },
        {
            path: '/about',
            name: 'about',
            component: About
        },
        {
            path: '/user/:id',
            name: 'user',
            component: User
        },
        {
            path: '/image/:id',
            name: 'image',
            component: Image
        }
    ]
})
