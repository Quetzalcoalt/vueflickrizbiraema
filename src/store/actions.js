import mutations from './mutation-types'
import actions from './action-types'
import apiService from '../api/flickr-service'

export default {
    [actions.GET_INITIAL_IMAGES]({ commit }, page) {
        // let images = []
        apiService
            .getMostRecentImages(page)
            .then(res => {
                //console.log(res.data.photos.photo)
                fetchingUserDataAndImageData(res, commit)
            })
            .then(() => {
                commit(mutations.MAIN_PAGE_LOADED)
                commit(mutations.SET_API_SERVER_ERROR, false)
                commit(mutations.LOADING_MORE_IMAGES, false)
            })
            .catch(error => {
                console.log(error)
                commit(mutations.SET_API_SERVER_ERROR, true)
            })
    },
    [actions.GET_MORE_IMAGES]({ commit }, page) {
        commit(mutations.LOADING_MORE_IMAGES, true)
        // let images = []
        apiService
            .getMostRecentImages(page)
            .then(res => {
                //console.log(res.data.photos.photo)
                fetchingUserDataAndImageData(res, commit)
            })
            .then(() => {
                commit(mutations.MAIN_PAGE_LOADED)
                commit(mutations.SET_API_SERVER_ERROR, false)
                commit(mutations.LOADING_MORE_IMAGES, false)
            })
            .catch(error => {
                console.log(error)
                commit(mutations.SET_API_SERVER_ERROR, true)
            })
    },

    [actions.GET_IMAGE_DATA]({ commit }, imageId) {
        apiService
            .getImageData(imageId)
            .then(res => {
                let photo = res.data.photo
                let name = ''
                if (photo.owner.username == '') {
                    name = photo.owner.realname
                } else {
                    name = photo.owner.username
                }
                let image = {
                    dateTaken: photo.dates.taken,
                    datePosted: photo.dates.posted,
                    title: photo.title._content,
                    description: photo.description._content,
                    views: photo.views,

                    userID: photo.owner.nsid,
                    username: name,
                    avatarUrl:
                        'http://farm' +
                        photo.owner.iconfarm +
                        '.staticflickr.com/' +
                        photo.owner.iconserver +
                        '/buddyicons/' +
                        photo.owner.nsid +
                        '.jpg',
                    imageID: photo.id,
                    imageUrl:
                        'https://farm' +
                        photo.farm +
                        '.staticflickr.com/' +
                        photo.server +
                        '/' +
                        photo.id +
                        '_' +
                        photo.secret +
                        '_h.jpg'
                }
                commit(mutations.SET_CURRENT_IMAGE, image)
                commit(mutations.SET_API_SERVER_ERROR, false)
            })
            .catch(error => {
                console.log(error)
                commit(mutations.SET_API_SERVER_ERROR, true)
            })
    },
    [actions.GET_USER_DATA]({ commit }, userId) {
        apiService
            .getUserData(userId)
            .then(res => {
                let person = res.data.person
                let name = getName(person)
                let user = {
                    description: person.description._content,
                    photosCount: person.photos.count._content,
                    username: name,
                    avatarUrl:
                        'http://farm' +
                        person.iconfarm +
                        '.staticflickr.com/' +
                        person.iconserver +
                        '/buddyicons/' +
                        person.nsid +
                        '.jpg'
                }
                commit(mutations.SET_CURRENT_USER, user)
                commit(mutations.SET_API_SERVER_ERROR, false)
            })
            .catch(error => {
                console.log(error)
                commit(mutations.SET_API_SERVER_ERROR, true)
            })
    },
    [actions.GET_IMAGES_WITH_TAGS]({ commit }, data) {
        //let images = []

        apiService
            .getImagesWithTags(data.tags, data.page)
            .then(res => {
                //console.log(res.data.photos.photo)
                fetchingUserDataAndImageData(res, commit)
            })
            .then(() => {
                commit(mutations.SET_API_SERVER_ERROR, false)
                commit(mutations.LOADING_MORE_IMAGES, false)
            })
            .catch(error => {
                console.log(error)
                commit(mutations.SET_API_SERVER_ERROR, true)
            })
    },
    [actions.GET_MORE_IMAGES_WITH_TAGS]({ commit }, data) {
        //let images = []
        commit(mutations.LOADING_MORE_IMAGES, true)
        apiService
            .getImagesWithTags(data.tags, data.page)
            .then(res => {
                //console.log(res.data.photos.photo)
                fetchingUserDataAndImageData(res, commit)
            })
            .then(() => {
                commit(mutations.SET_API_SERVER_ERROR, false)
                commit(mutations.LOADING_MORE_IMAGES, false)
            })
            .catch(error => {
                console.log(error)
                commit(mutations.SET_API_SERVER_ERROR, true)
            })
    },
    [actions.CLEAR_CURRENT_IMAGE]({ commit }) {
        commit(mutations.CLEAR_CURRENT_IMAGE)
    },
    [actions.CLEAR_CURRENT_USER]({ commit }) {
        commit(mutations.CLEAR_CURRENT_USER)
    },
    [actions.CLEAR_IMAGES_LIST]({ commit }) {
        commit(mutations.CLEAR_IMAGES_LIST)
    }
}

function fetchingUserDataAndImageData(res, commit) {
    res.data.photos.photo.forEach(img => {
        apiService.getUserData(img.owner).then(data => {
            let person = data.data.person
            let name = getName(person)
            let image = {
                imageID: img.id,
                userID: img.owner,
                username: name,
                avatarUrl:
                    'http://farm' +
                    person.iconfarm +
                    '.staticflickr.com/' +
                    person.iconserver +
                    '/buddyicons/' +
                    person.nsid +
                    '.jpg',
                title: img.title,
                imageUrl:
                    'https://farm' +
                    img.farm +
                    '.staticflickr.com/' +
                    img.server +
                    '/' +
                    img.id +
                    '_' +
                    img.secret +
                    '_z.jpg'
            }
            //images.push(image)
            commit(mutations.ADD_MORE_IMAGES, image)
        })
    })
}

function getName(person) {
    if (person.username._content === undefined) {
        return person.realname._content
    } else {
        return person.username._content
    }
}
