import mutations from './mutation-types'

export default {
    [mutations.SET_CURRENT_IMAGE](state, image) {
        state.currentImage = image
    },
    [mutations.SET_CURRENT_USER](state, user) {
        state.currentUser = user
    },
    [mutations.MAIN_PAGE_LOADED](state) {
        state.mainPageLoaded = true
    },
    [mutations.CLEAR_CURRENT_IMAGE](state) {
        state.currentImage = {}
    },
    [mutations.CLEAR_CURRENT_USER](state) {
        state.currentUser = {}
    },
    [mutations.CLEAR_IMAGES_LIST](state) {
        state.imageList = []
    },
    [mutations.SET_API_SERVER_ERROR](state, value) {
        state.apiServerError = value
    },
    [mutations.ADD_MORE_IMAGES](state, image) {
        state.imageList.push(image)
        // console.log('images from state' + state.mostRecentImages)
    },
    [mutations.LOADING_MORE_IMAGES](state, value) {
        state.loadingMoreImages = value
    }
}
