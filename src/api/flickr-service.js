import axios from 'axios'
import constants from '../constants'

export default {
    getMostRecentImages(page) {
        if (page === undefined) {
            return axios.get(constants.FLICKR_API_URL, {
                params: {
                    method: constants.GET_METHOD_INTERESTINGS,
                    api_key: constants.KEY,
                    per_page: constants.PER_PAGE,
                    page: '1',
                    format: 'json',
                    nojsoncallback: '1'
                }
            })
        } else {
            return axios.get(constants.FLICKR_API_URL, {
                params: {
                    method: constants.GET_METHOD_INTERESTINGS,
                    api_key: constants.KEY,
                    per_page: constants.PER_PAGE,
                    page: page,
                    format: 'json',
                    nojsoncallback: '1'
                }
            })
        }
    },
    getUserData(userId) {
        // return axios.get(
        //     'https://api.flickr.com/services/rest/?method=flickr.people.getInfo&api_key=' +
        //         constants.Key +
        //         '&user_id=' +
        //         userId +
        //         '&format=json&nojsoncallback=1'
        // )
        return axios.get(constants.FLICKR_API_URL, {
            params: {
                method: constants.GET_METHOD_PEOPLE_INFO,
                api_key: constants.KEY,
                user_id: userId,
                format: 'json',
                nojsoncallback: '1'
            }
        })
    },
    getImageData(photoId) {
        return axios.get(constants.FLICKR_API_URL, {
            params: {
                method: constants.GET_METHOD_PHOTOS_INFO,
                api_key: constants.KEY,
                photo_id: photoId,
                format: 'json',
                nojsoncallback: '1'
            }
        })
    },
    getImagesWithTags(tags, page) {
        tags = tags.replace(' ', ',')
        if (page === undefined) {
            return axios.get(constants.FLICKR_API_URL, {
                params: {
                    method: constants.GET_METHOD_PHOTOS_SEARCH,
                    api_key: constants.KEY,
                    tags: tags,
                    per_page: constants.PER_PAGE,
                    page: '1',
                    format: 'json',
                    nojsoncallback: '1'
                }
            })
        } else {
            return axios.get(constants.FLICKR_API_URL, {
                params: {
                    method: constants.GET_METHOD_PHOTOS_SEARCH,
                    api_key: constants.KEY,
                    tags: tags,
                    per_page: constants.PER_PAGE,
                    page: page,
                    format: 'json',
                    nojsoncallback: '1'
                }
            })
        }
    }
}
